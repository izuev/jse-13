package ru.tsc.izuev.tm.constant;

import java.text.DecimalFormat;

public final class FormatConst {

    public static final long KILOBYTE = 1024;

    public static final long MEGABYTE = KILOBYTE * 1024;

    public static final long GIGABYTE = MEGABYTE * 1024;

    public static final long TERABYTE = GIGABYTE * 1024;

    public static final String NAME_BITES = "B";

    public static final String NAME_BITES_LONG = "Bites";

    public static final String NAME_KILOBYTES = "KB";

    public static final String NAME_MEGABYTES = "MB";

    public static final String NAME_GIGABYTES = "GB";

    public static final String NAME_TERABYTES = "TB";

    public static final String SEPARATOR = " ";

    public static final DecimalFormat FORMAT = new DecimalFormat("#.###");

    private FormatConst() {
    }

}
