package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
