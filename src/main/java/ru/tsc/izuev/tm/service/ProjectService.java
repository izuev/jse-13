package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.api.service.IProjectService;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Project;

import java.util.Collections;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= projectRepository.size()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= projectRepository.size()) return null;
        if (status == null) return null;
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) return null;
        return projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Integer size() {
        return projectRepository.size();
    }

    @Override
    public boolean notExistsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return projectRepository.notExistsById(id);
    }

}
