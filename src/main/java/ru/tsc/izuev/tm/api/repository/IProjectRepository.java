package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findById(String id);

    Project findByIndex(Integer index);

    boolean notExistsById(String id);

    Project add(Project project);

    void clear();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Integer size();

}
