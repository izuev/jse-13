package ru.tsc.izuev.tm.api.repository;

import ru.tsc.izuev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findById(String id);

    Task findByIndex(Integer index);

    Task add(Task task);

    void clear();

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Integer size();

}
