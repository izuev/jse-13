package ru.tsc.izuev.tm.repository;

import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task : tasks) {
            if (projectId.equals(task.getProjectId())) tasksByProjectId.add(task);
        }
        return tasksByProjectId;
    }

    @Override
    public Task findById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        Task task = findById(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        Task task = findByIndex(index);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Integer size() {
        return tasks.size();
    }

}
