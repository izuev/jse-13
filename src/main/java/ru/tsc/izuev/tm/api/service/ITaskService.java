package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
