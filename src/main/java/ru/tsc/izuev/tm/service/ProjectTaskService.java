package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.api.service.IProjectTaskService;
import ru.tsc.izuev.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    public final IProjectRepository projectRepository;

    public final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectRepository.notExistsById(projectId)) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void remoteProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (projectRepository.notExistsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectRepository.notExistsById(projectId)) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
